# Changelog

## 1.4.8 - 2025-10-02

### Fix - Feat

feat: Ajout page 404
fix: Correction du style fontawsome dans le modele icone
Feat: Ajout Fil d'Ariane

## 1.4.7 - 2024-12-04

### Fix
- Correction positionnement des codes optionnels et criètres de tri de boucles

## 1.4.6 - 2024-11-22

### Feat
- 8c77c62 Feat: Pagination pages sommaire, article et rubrique
- ecf6934 Feat: Ajout connexion espace privé dans footer

## 1.4.5 - 2024-11-01

### Refactor
- a4b2d0a Ajout section pour mise en page sidebar auteur
### Fix
- be09728 Le bandeau n'est plus obligatoire dans la configuration du plugin
### Feat
- 2297e3a Ajout Connection/Déconnection dans le footer

## 1.4.4 - 2024-10-13

### Feat
- Ajout page auteur et références auteurs dans les articles
### Refactor
- Modification positionnement du formulaire de recherche
### Remove
- Référence SPIP dans le footer
### Fix
- Boucles articles et rubriques triées par num_titre en premier


## 1.4.3 - 2024-10-06

### Fix
-  Boucle avec critère {page} dans un inclure pour rendre le Plugin Page vraiment non nécessaire

## 1.4.2 - 2024-09-15

### Fix
- Correction navigation par mots clés

## 1.4.1 - 2024-09-14

### Feat
- #2 Modification footer avec formulaire de recherche
- #3 #4 Ajout des pages mot et site
- Configuration de la slidebar

## 1.3.7 - 2024-06-05

### Fix

- necessite le plugin "page unique"

## 1.3.6 - 2024-05-29

### Fix

- Ajout |timestamp à perso.css
- Redimensionnement image header

### Feat

- Libellé spécifique configurable pour le titre de la barre latérale des pages rubrique et article
- Copyright supplémentaire configurable dans le pied de page

## 1.3.5 - 2024-04-03

### Fix

- Issue #1 : Correction de la page "mentions" dans le footer et le plugin "Page unique" passe simplement en utilise

## 1.3.4 - 2024-04-02

### Fix

- Ajout de la feuille de style css/clear.css pour affichage du portfolio et déplacement de celui-ci dans l'article

## 1.3.3 - 2023-11-28

### Fix

- necessite le plugin "page unique"

### Feat

- Tri par date inverse des articles de la page rubrique + articles
- Article "editorial" uniquement sur page sommaire

# 1.3.2 - 2023-11-18

### Fix

- correction logo dans pages Sommaire et Rubrique


## 1.3.1 - 2023-11-18

### Fix

- Correction logo et url dans pages sommaire et rubriques
- Utilisation de la balise #CHEMIN pour les script js dans le footer du theme zerofour

## 1.3.1 - 2023-11-01

### Fix

- Correction bouton "lire la suite" dans sommaire

### Changed

- Remplacement de la police Fontawesome par le plugin SPIP Fontawesome

## 1.3.0 - 2023-10-31

### Added

- Ajout des 2 modèles `<bouton>` et `<icone>
- Ajout d'un `CHANGELOG.md` et mise à jour du `README.md`

### Changed

- Remplacement de la police Fontawesome par le plugin SPIP Fontawesome