<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accueil' => 'Accueil',
	'archives' => 'Archives',

	// B
	'bouton_subscribe' => 'S\'abonner',

	// C
	'contact' => 'Contact',
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'contenu_site' => 'Contenu du site',


	// E
    'explication_articles_home_avant' => "La page sommaire affiche les rubriques de premier niveau. Vous pouvez avoir besoin d'ajouter un ou des articles AVANT cette liste de rubriques." ,
    'explication_articles_home_apres' => "La page sommaire affiche les rubriques de premier niveau. Vous pouvez avoir besoin d'ajouter un ou des articles APRES cette liste de rubriques." ,
	'explication_articles_en_avant' => "Le milieu de page met aussi en avant des articles spécifiques." ,
	'explication_rubriques_en_avant' => "Le milieu de page met en avant les articles d'une ou plusieurs rubriques." ,
	'explication_config_contenu' => "Le squelette utilise les sous titres pour les articles, n'oubliez pas de les activer dans <a href='?exec=configurer_contenu'>configurer contenu</a>",
	'explication_config_menu' => "Le nom du site apparait dans le menu",
	'explication_config_bandeau' => "Le nom du site apparait sur l'image sur fond",
	'explication_doc_bandeau' => "L'id du document qui sera utilisé pour l'image du bandeau. Le thème original utilise une image de taille <b>2000 × 656 pixels</b> ",
	'explication_text_blog' => "Texte du surtitre du blog (qui prendra la balise H2). 'Articles les plus récents' par défaut",
	'explication_text_sous_rubrique' => "Texte du surtitre des sous-rubriques (qui prendra la balise H2). 'Rubriques' par défaut",
	'explication_text_rubrique' => "Texte du surtitre des articles (qui prendra la balise H2). 'Articles' par défaut",
	'explication_text_article' => "Texte du surtitre des articles de la même rubrique (qui prendra la balise H2). 'Dans la même rubrique' par défaut",
	'explication_text_lateral' => "Texte du surtitre des rubriques dans la barre latérale. 'Rubriques' par défaut",
	'explication_rubrique_blog' => "Rubrique(s) présentée(s) sous forme de blog dans le bas de la page.",
	'explication_affiche_icones' => "Les icônes sont affichées avant le titre des articles et des rubriques",
	'explication_icone_articles' => "Code de l'icône <a href='https://fontawesome.com/v5/search' target='_blank'>FontAwesome</a> des articles ",
	'explication_icone_rubriques' => "Code de l'icône <a href='https://fontawesome.com/v5/search' target='_blank'>FontAwesome</a> des rubriques ",
	'explication_article_editorial' => "Article de l'éditorial du bas de la page.",
	'explication_text_editorial' => "Texte du surtitre de l'éditorial (qui prendra la balise H2). 'Editorial' par défaut",
	'explication_fond_couleur' => "Choisissez votre couleur de fond (par défaut #3B3E45)",
	'explication_copyright' => "Copyright supplémentaire de pied de page",
	'explication_configuration' => "Configuration des pages",
	'explication_auteur' => "Affichage de l'auteur dans les articles qui renvoie vers la page auteur",
	'editorial' => "Editorial",

	// H
	'html5up_zerofour_titre' => 'Configurer html5up zerofour',	
	
	// L
    'label_articles_home_avant' => "Article(s) avant liste des secteurs",
    'label_articles_home_apres' => "Article(s) après liste des secteurs",
	'label_articles_en_avant' => "Article(s) en avant",
	'label_rubriques_en_avant' => "Rubrique(s) en avant",
	'label_config_contenu' => "Sous-titre",
	'label_config_menu' => "Nom dans le menu",
	'label_config_bandeau' => "Nom sur l'image",
	'label_doc_bandeau' => "Id image bandeau",
	'label_text_sous_rubrique' => "Surtitre des sous-rubriques",
	'label_text_rubrique' => "Surtitre des articles",
	'label_text_article' => "Surtitre des articles de la même rubrique",
	'label_text_lateral' => "Surtitre des rubriques dans la barre latérale",
	'label_text_blog' => "Surtitre du blog",
	'label_rubrique_blog' => "Rubrique(s) du blog",
	'label_affiche_icones' => "Affiche les icônes",
	'label_icone_articles' => "Icône articles",
	'label_icone_rubriques' => "Icône rubriques",
	'label_copyright' => "Copyright",
	'legend' => "Préambule",
	'legend_sommaire' => "Paramétrage page Sommaire ",
	'legend_rubrique' => "Paramétrage page Rubrique ",
	'legend_article' => "Paramétrage page Article ",
	'lire_suite' => "Lire la suite",
	'label_article_editorial' => "Article éditorial",
	'label_text_editorial' => "Surtitre de l'éditorial",
	'label_fond_couleur' => "Votre couleur",
	'label_configuration' => "Configuration",
	'label_auteur' => "Affichage de l'auteur",
	'lire_suite' => 'Lire la suite',
	'legend_footer' => "Paramétrage Footer",

	// N
	'nous_suivre' => 'Nous suivre',

	// R
	'read_more' => 'Lire la suite',

	// T
	'titre_newsletters' => "Newsletters",
	'texte_newsletters' => "Pour avoir des nouvelles régulières, inscrivez votre mail ",
	'texte_config_bandeau' => "Le nom du site apparait sur l'image de fond",
	'texte_config_menu' => "Le nom du site apparait dans le menu",
	'texte_affiche_icones' => "Les icônes sont affichées",
	'texte_auteur' => "Affichage de l'auteur",
	'titre_configurer_html5up_zerofour' => 'Configurer Html5up Zerofour',
	'titre_page_configurer_html5up_zerofour' => 'Configurer Html5up Zerofour',
	'rester_en_contact' => 'Rester en contact',

	// H
	'html5up_zerofour_titre' => 'Html5Up zerofour',


);
