<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// H
	'html5up_zerofour_description' => '',
	'html5up_zerofour_nom' => 'Html5Up Zerofour',
	'html5up_zerofour_slogan' => 'Squelette adapté du thème TXT de HTML5 Zerofour',
];
